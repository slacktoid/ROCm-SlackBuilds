#!/bin/bash
set -e
VERSION=${VERSION:-6.0.0}
cd $(dirname $0) ; CWD=$(pwd)
PKGDIR=${CWD}/pkg

if [ "$1" = "--download" ]; then
  VERSION=$VERSION ./download.sh
fi

build_deps ()
{
	APP=$1
	VER=$2
	if [ ! -f ${PKGDIR}/${APP}-${VER}-x86_64-1_slktd.tgz ]; then
		OUTPUT=${CWD}/pkg ./${APP}.SlackBuild
	fi
	
}

build (){
	APP=$1
	if [ ! -f ${PKGDIR}/${APP}-${VERSION}-x86_64-1_slktd.tgz ]; then
		VERSION=$VERSION OUTPUT=${CWD}/pkg ./${APP}.SlackBuild
	fi
	upgradepkg --install-new --reinstall ${PKGDIR}/${APP}-${VERSION}-x86_64-1_slktd.tgz 
}

# deps so far are independent
declare -A DEPS
DEPS=(
	["python3-cppheaderparser"]="2.7.4"
)

# order is important here
# "hipcc" ## weird process with this
APPLIST=(
	"rocm-llvm"
	"rocm-cmake"
	"rocm-device-libs"
	"rocm-core"
	"rocm-comgr"
	"hsakmt-roct"
	"hsa-rocr"
	"rocminfo"
	"hip-runtime-amd"
	"rocm-opencl-runtime"
	"rocblas"
)

for d in "${!DEPS[@]}"
do
	build_deps $d ${DEPS[$d]}
done

for i in "${APPLIST[@]}"
do
	build $i
done

# in case it all breaks
# if [ ! -f ${PKGDIR}/rocm-llvm-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./rocm-llvm.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/rocm-llvm-${VERSION}-x86_64-1_slktd.tgz 
#
# if [ ! -f ${PKGDIR}/rocm-cmake-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./rocm-cmake.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/rocm-cmake-${VERSION}-x86_64-1_slktd.tgz 
#
# if [ ! -f ${PKGDIR}/rocm-device-libs-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./rocm-device-libs.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/rocm-device-libs-${VERSION}-x86_64-1_slktd.tgz 
#
# if [ ! -f ${PKGDIR}/rocm-core-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./rocm-core.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/rocm-core-${VERSION}-x86_64-1_slktd.tgz
#
# if [ ! -f ${PKGDIR}/rocm-comgr-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./rocm-comgr.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/rocm-comgr-${VERSION}-x86_64-1_slktd.tgz
#
# if [ ! -f ${PKGDIR}/hsakmt-roct-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./hsakmt-roct.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/hsakmt-roct-${VERSION}-x86_64-1_slktd.tgz 
#
# if [ ! -f ${PKGDIR}/hsa-rocr-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./hsa-rocr.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/hsa-rocr-${VERSION}-x86_64-1_slktd.tgz
#
# if [ ! -f ${PKGDIR}/rocminfo-${VERSION}-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./rocminfo.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/rocminfo-${VERSION}-x86_64-1_slktd.tgz
#
# if [ ! -f ${PKGDIR}/python3-cppheaderparser-2.7.4-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./python3-cppheaderparser.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/python3-cppheaderparser-2.7.4-x86_64-1_slktd.tgz
#
# if [ ! -f ${PKGDIR}/hip-runtime-amd-6.0.0-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./hip-runtime-amd.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/hip-runtime-amd-6.0.0-x86_64-1_slktd.tgz
#
# if [ ! -f ${PKGDIR}/rocm-opencl-runtime-6.0.0-x86_64-1_slktd.tgz ]; then
#   OUTPUT=${CWD}/pkg ./rocm-opencl-runtime.SlackBuild
# fi
# upgradepkg --install-new --reinstall ${PKGDIR}/rocm-opencl-runtime-6.0.0-x86_64-1_slktd.tgz
