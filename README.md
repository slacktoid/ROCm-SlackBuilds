# ROCm-SlackBuilds

AMD ROCm runtime+drivers needed for Slackware

For 5.5.1 go to https://www.linuxgalaxy.org/kingbeowulf/rocm-opencl-runtime-for-slackware64/
(You could even change the versions to 5.7 and it could work? fingers crossed)

This is still WIP

## Dependencies

Install the following dependencies from slackbuilds.org/

* `numactl`
* `perl-File-Which`
* `msgpack-c` 
* `python3-setuptools-opt` 
* `python3-joblib`

## Build Order

1.  rocm-llvm
2.  rocm-cmake
3.  rocm-device-libs
4.  rocm-core
5.  rocm-comgr
6.  hsakmt-roct
7.  hsa-rocr
8.  rocminfo (issues on 15.0)
9.  python3-cppheaderparser (dep for hip-runtime-amd)
10. hip-runtime-amd
11. rocm-opencl-runtime
12. rocblas (Takes a long time to build, please report any issues)

n. profit!

Working on the rest

## Issues

1.  `PRGNAM` and `SRCNAM` are wrongly specified. (shouldn't break anything as long as you delete the `TMP` directory before rebuilds)
2.  `slack-desc` files are missing, Currently embedded in build file
3.  Too many packages, ideally build to fewer usable packages
4.  Add the `LICENSE` of each app
5.  Compress man pages where applicable
6.  `info` files
