#!/bin/bash
set -e
cd $(dirname $0) ; CWD=$(pwd)
VERSION=${VERSION:-5.5.0}
PKGDIR=${CWD}/pkg

for pkg in $PKGDIR/*_slktd.t?z; do
  removepkg $pkg
done
