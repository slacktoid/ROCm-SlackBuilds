
VERSION=${VERSION:-5.5.0}

ln -s ../src/rocm-llvm-$VERSION.tar.gz llvm-16.0.3.src.tar.gz
lftp -c "open https://mirrors.slackware.com/slackware/slackware-current/source/d/llvm/; mirror --verbose --exclude [Ss]lack[Bb]uild --exclude llvm-16.0.3.src.tar.xz"
