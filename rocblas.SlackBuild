#!/bin/bash

# Slackware build script for rocblas

# Copyright 2023 slacktoid Earth
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PRGNAM=rocblas
VERSION=${VERSION:-6.0.0}

TARROC=rocblas
VERROC=${VERROC:-4.0.0}
TARTEN=tensile
VERTEN=${VERTEN:-4.39}

SRCROC=rocBLAS-rocm-${VERSION}
SRCTEN=Tensile-rocm-${VERSION}

BUILD=${BUILD:-1}
TAG=${TAG:-_slktd}		# the "_SBo" is required
PKGTYPE=${PKGTYPE:-tgz}
NPROCS=${NPROCS:-9}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
	case "$( uname -m )" in
		i?86) ARCH=i586 ;;
		arm*) ARCH=arm ;;
		# Unless $ARCH is already set, use uname -m for all other archs:
		*) ARCH=$( uname -m ) ;;
	esac
fi

if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
	echo "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
	exit 0
fi


TMP=${TMP:-/tmp/ROCm-SBo}		# For consistency's sake, use this
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}		# Drop the package in /tmp

if [ "$ARCH" = "i586" ]; then
	SLKCFLAGS="-O2 -march=i586 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
	SLKCFLAGS="-O2 -march=i686 -mtune=i686"
	LIBDIRSUFFIX=""
# NOTE: only tested on x86_64, `-L/opt/rocm/lib64` is being added as it was not building
elif [ "$ARCH" = "x86_64" ]; then
	SLKCFLAGS="-O2 -fPIC -fcf-protection=none -L/opt/rocm/lib64 -parallel-jobs=${NPROCS}"
	LIBDIRSUFFIX="64"
else
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX=""
fi

set -e # Exit on most errors

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf $PRGNAM-$VERSION
rm -rf $SRCTEN $SRCROC
tar xvf $CWD/src/$TARROC-$VERROC.tar.gz
tar xvf $CWD/src/$TARTEN-$VERTEN.tar.gz

# cd $SRCROC

cd $SRCTEN
# patch -Np1 -i ${CWD}/find-msgpack-5.patch
find -L . \
	\( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
	-o -perm 511 \) -exec chmod 755 {} \; -o \
	\( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
	-o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


# Your application will probably need different cmake flags; these are only
# examples.  You might use 'ccmake' to see the available flags...
cd ../$SRCROC
chown -R root:root .
find -L . \
	\( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
	-o -perm 511 \) -exec chmod 755 {} \; -o \
	\( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
	-o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;
mkdir -p build
cd build

# Compile source code for supported GPU archs in parallel
export HIPCC_COMPILE_FLAGS_APPEND="-parallel-jobs=${NPROCS}"
export HIPCC_LINK_FLAGS_APPEND="-parallel-jobs=${NPROCS}"
# export HIP_CLANG_HCC_COMPAT_MODE=1
export LD_LIBRARY_PATH="/opt/rocm/lib64/:${LD_LIBRARY_PATH}"
# -fcf-protection is not supported by HIP, see
# https://docs.amd.com/bundle/ROCm-Compiler-Reference-Guide-v5.4/page/Appendix_A.html
# -DCMAKE_C_FLAGS:STRING="$SLKCFLAGS" \
# -DCMAKE_CXX_FLAGS:STRING="$SLKCFLAGS" \
# -DLIB_SUFFIX=${LIBDIRSUFFIX} \
# -DTensile_CODE_OBJECT_VERSION=V3 \
HIPCC_COMPILE_FLAGS_APPEND="-parallel-jobs=${NPROCS}" \
HIPCC_LINK_FLAGS_APPEND="-parallel-jobs=${NPROCS}"  \
CXXFLAGS="${SLKCFLAGS} -fcf-protection=none" \
PATH="/opt/rocm/llvm/bin:/opt/rocm/bin:${PATH}" \
cmake \
	-G "Unix Makefiles" \
	-DCMAKE_C_FLAGS:STRING="$SLKCFLAGS" \
	-DCMAKE_CXX_FLAGS:STRING="$SLKCFLAGS" \
	-Wno-dev \
	-S "${TMP}/${SRCROC}" \
	-DCMAKE_BUILD_TYPE=None \
	-DCMAKE_CXX_COMPILER=/opt/rocm/bin/hipcc \
	-DCMAKE_INSTALL_PREFIX=/opt/rocm \
	-DCMAKE_PREFIX_PATH=/opt/rocm/llvm/lib/cmake/llvm \
	-Damd_comgr_DIR=/opt/rocm/lib${LIBDIRSUFFIX}/cmake/amd_comgr \
	-DBUILD_WITH_TENSILE=ON \
	-DTensile_LIBRARY_FORMAT=msgpack \
	-DCMAKE_TOOLCHAIN_FILE=toolchain-linux.cmake \
	-DBUILD_FILE_REORG_BACKWARD_COMPATIBILITY=ON \
	-DTensile_TEST_LOCAL_PATH="${TMP}/${SRCTEN}"

make -j${NPROCS}
make install DESTDIR=$PKG
cd ..

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

echo '/opt/rocm/$PKG/lib' > "$PRGNAM.conf"
install -Dm644 "$PRGNAM.conf" "$PKG/etc/ld.so.conf.d/$PRGNAM.conf"

# Strip binaries and libraries - this can be done with 'make install-strip'
# in many source trees, and that's usually acceptable, if not, use this:
find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
	| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress man pages
# If the man pages are installed to /usr/share/man instead, you'll need to 
# move them manually.
#find $PKG/usr/man -type f -exec gzip -9 {} \;
# for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

# Compress info pages and remove the package's dir file
# If no info pages are installed by the software, don't leave this in the script
#rm -f $PKG/usr/info/dir
#gzip -9 $PKG/usr/info/*.info*

# Remove perllocal.pod and other special files that don't need to be installed,
# as they will overwrite what's already on the system.  If this is not needed,
# remove it from the script.
find $PKG -name perllocal.pod -o -name ".packlist" -o -name "*.bs" | xargs rm -f || true

# Copy program documentation into the package
# The included documentation varies from one application to another, so be sure
# to adjust your script as needed
# Also, include the SlackBuild script in the documentation directory
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

# Copy the slack-desc (and a custom doinst.sh if necessary) into ./install
#mkdir -p $PKG/install
#cat $CWD/slack-desc > $PKG/install/slack-desc
#cat $CWD/doinst.sh > $PKG/install/doinst.sh

# Make the package; be sure to leave it in $OUTPUT
# If package symlinks need to be created during install *before*
# your custom contents of doinst.sh runs, then add the -p switch to
# the makepkg command below -- see makepkg(8) for details
cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
