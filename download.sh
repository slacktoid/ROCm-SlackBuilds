#!/bin/bash

VERSION=${VERSION:-6.0.0}
PYCPPVERSION=${PYCPPVERSION:-2.7.4}
PROJECTS=(llvm-project rocm-cmake)

cd $(dirname $0) ; CWD=$(pwd)
mkdir -p .${CWD}/src 
mkdir -p .${CWD}/pkg

# these are downloads where a tar file corresponds to a slackbuild
wget -nc https://github.com/RadeonOpenCompute/llvm-project/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocm-llvm-${VERSION}.tar.gz
wget -nc https://github.com/RadeonOpenCompute/rocm-cmake/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocm-cmake-${VERSION}.tar.gz
wget -nc https://github.com/RadeonOpenCompute/ROCm-Device-Libs/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocm-device-libs-${VERSION}.tar.gz
wget -nc https://github.com/RadeonOpenCompute/ROCm-CompilerSupport/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocm-comgr-${VERSION}.tar.gz
wget -nc https://github.com/RadeonOpenCompute/ROCR-Runtime/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/hsa-rocr-${VERSION}.tar.gz
wget -nc https://github.com/RadeonOpenCompute/ROCT-Thunk-Interface/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/hsakmt-roct-${VERSION}.tar.gz
wget -nc https://github.com/RadeonOpenCompute/rocminfo/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocminfo-${VERSION}.tar.gz
wget -nc https://github.com/ROCm-Developer-Tools/HIPCC/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/hipcc-${VERSION}.tar.gz

# For RocBLAS
wget -nc https://github.com/ROCmSoftwarePlatform/rocBLAS/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocblas-4.0.0.tar.gz
wget -nc https://github.com/ROCmSoftwarePlatform/Tensile/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/tensile-4.39.tar.gz

# For roctracer
wget -nc https://github.com/ROCm/roctracer/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocm-roctracer-${VERSION}.tar.gz

# patches
wget -nc https://raw.githubusercontent.com/archlinux/svntogit-community/packages/rocblas/trunk/find-msgpack-5.patch
wget -nc https://gitlab.archlinux.org/archlinux/packaging/packages/hsakmt-roct/-/raw/main/hsakmt-global-visibility.patch
wget -nc https://gitlab.archlinux.org/archlinux/packaging/packages/rocm-cmake/-/raw/main/rocm-cmake-old-policy-cmp0079.patch
wget -nc https://gitlab.archlinux.org/archlinux/packaging/packages/rocm-device-libs/-/raw/main/rocm-device-libs-rm-gfx700.patch
wget -nc https://gitlab.archlinux.org/archlinux/packaging/packages/rocblas/-/raw/main/find-msgpack-5.patch

# ROCm OpenCL Runtime
wget -nc https://github.com/RadeonOpenCompute/ROCm-OpenCL-Runtime/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/rocm-opencl-runtime-${VERSION}.tar.gz
# wget -nc https://github.com/ROCm-Developer-Tools/ROCclr/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/ROCclr-${VERSION}.tar.gz
wget -nc https://github.com/ROCm/clr/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/ROCclr-${VERSION}.tar.gz

# just for hip-runtime-amd
wget -nc https://files.pythonhosted.org/packages/3c/ba/d8d168a4b54cae66eaf13d1d9197ca9349c94653815e061f79e7eed86c01/CppHeaderParser-${PYCPPVERSION}.tar.gz -O ./src/CppHeaderParser-${PYCPPVERSION}.tar.gz
wget -nc https://github.com/ROCm-Developer-Tools/HIP/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/hip-runtime-amd-hip-${VERSION}.tar.gz
# replaced by clr
# wget -nc https://github.com/RadeonOpenCompute/ROCm-OpenCL-Runtime/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/hip-runtime-amd-ocl-${VERSION}.tar.gz
# wget -nc https://github.com/ROCm-Developer-Tools/ROCclr/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/hip-runtime-amd-rocclr-${VERSION}.tar.gz
# wget -nc https://github.com/ROCm-Developer-Tools/hipamd/archive/refs/tags/rocm-${VERSION}.tar.gz -O ./src/hip-runtime-amd-hipamd-${VERSION}.tar.gz

