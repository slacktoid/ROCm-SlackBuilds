# variables used for projects built with hipcc
export HIP_PLATFORM=amd
export HIP_RUNTIME=rocclr
export HIP_COMPILER=clang
export HIP_CLANG_PATH=/opt/rocm/llvm/bin
export DEVICE_LIB_PATH=/opt/rocm/amdgcn/bitcode
export HIP_DEVICE_LIB_PATH=/opt/rocm/amdgcn/bitcode
export HSA_PATH=/opt/rocm/hsa
export LLVM_PATH=/opt/rocm/llvm/
export HIP_PATH=/opt/rocm/hip
export ROCM_PATH=/opt/rocm
#export HCC_AMDGPU_TARGET=$DEB_HIP_ARCHITECTURES # workaround for hipcc path issues
